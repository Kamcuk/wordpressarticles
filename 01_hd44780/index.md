---
author: kamilcukrowski
brand: xebia.com
date: 2014-09-10 10:39:40+02:00
email: kamilcukrowski@gmail.com
focus-keywords: hd44780
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/242
slug: hd44780
status: draft
subtitle: ''
title: HD44780
---

Hi.

One of my first big projects was creating a HD44780 connected displays to
parallel port of PC. I had 9 HD44780 displays each 2 rows per 40 columns.

The first version was using CMOS 4017 to switch between an enabled bit of HD44780
controllers. That way I could connect multiple displays to one parallel port driver
of my PC. I have written a kernel module to do that. The sources of that
kernel module still lives in [this repository](https://gitlab.com/Kamcuk/qpq/-/tree/master/hd44780mods/lcdmod-binary/trunk?ref_type=heads).

Then the second was to take a microcontroller and connect the devices
to it. I took `PIC18F2550` that has USB support and connected it via
USB to PC, exposing with also a kernel driver that exposed `/dev/uhcX`
interface. The sources are available at
[Kamilcuk/pic_projects](https://github.com/Kamilcuk/pic_projects/blob/master/xc8_pic/10_hd44780_test/main.c).

It was really fun to do it. This is a project from around 2012-1015

<a href="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00241.jpg">
<img class="alignnone size-medium wp-image-25" src="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00241-300x168.jpg" alt="DSC_0024" width="300" height="168" />
</a>
 <a href="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00251.jpg">
<img class="alignnone size-medium wp-image-27" src="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00251-300x168.jpg" alt="DSC_0025" width="300" height="168" />
</a>
 <a href="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00261.jpg">
<img class="alignnone size-medium wp-image-29" src="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00261-300x168.jpg" alt="DSC_0026" width="300" height="168" />
</a>
 <a href="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00271.jpg">
<img class="alignnone size-medium wp-image-30" src="http://karta.dyzio.pl/pro/wp-content/uploads/2014/09/DSC_00271-300x168.jpg" alt="DSC_0027" width="300" height="168" />
</a>
