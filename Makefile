# Makefile

MAKEFLAGS = -rR --warn-unused-variables
SHELL = bash
HOST = thehumancode.ovh
HOST = kernelodyssey.ovh
WP = wp-md posts $1 --host $(HOST)
ARGS ?=
posts = $(patsubst %/,%,$(dir $(wildcard */index.md)))

all: upload

_download:
	mkdir -vp ./_build/raw/ ./_build/download/
	$(call WP, download) --directory _build/raw/
	dups=$$(basename -a _build/raw/*/*/*/ | sort | uniq -d); [[ -z "$$dups" ]]
	rm -rf _build/download/*
	mv _build/raw/*/*/* _build/download/
	rm -rf _build/raw/
_download_sync:
	echo rsync -icrvh --exclude _build _build/download/ ./
_download_clear:
	echo rm -rf _build/raw/ _build/download/

_diff:
	diff -r --exclude _build -x md ./ _build/download/
diff:
	@$(MAKE) _download
	@$(MAKE) _diff

define upload_dir
_build/uploaded_$1.stamp: $$(shell find ./$1/ -type f)
	$(call WP, upload) ./$1/
	@mkdir -vp _build
	@touch $$@
upload_$1: _build/uploaded_$1.stamp
endef
$(foreach i,$(posts),$(eval $(call upload_dir,$(i))))
upload: $(foreach i,$(posts),upload_$(i))

