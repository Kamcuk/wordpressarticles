---
author: kamilcukrowski
brand: xebia.com
date: 2014-10-22 11:47:29+02:00
email: kamilcukrowski@gmail.com
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/237
slug: rock-paper-scissors
status: publish
subtitle: ''
title: Rock paper scissors
---

This program calculates the probability of the next human move by judging the
previous moves of the user. It works really well. After enough many moves I
allay loose to it. To win, you have to play perfectly.


```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>

char getch() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}

/*
 * 
 * win>defeat
 * 1>3
 * 3>2
 * 2>1
 * */
 
#define KAMIEN 0
#define PAPIER 1
#define NOZYCE 2

struct move_s {
	char com : 3; // computer
	char hum : 3; // human
	char empty : 2;
};
struct move_s *mv = NULL;
int mv_num = 0;
struct {
	int com;
	int hum;
	int even;
} wins = {0,0,0};

void safe_exit(char *str)
{
	printf(str);
	printf("\n");
	if ( mv != NULL ) 
		free(mv);
	exit(0);
}

static void sig_hup ()
{
	printf("Cought signal. Shutting down.\n");
	safe_exit("");
}


char get_com_move()
{
	int p[3] = {0,0,0};
	int i;
	int next_mv;
	int last_mv;
	
	// predict moves based on probality of a next move 
	
	
	// well, first round
	if ( mv_num == 0 || mv_num == 1 )
		return rand()%3;
	
	last_mv = mv[mv_num-1].hum;
	
	// what was this move?
	for(i=0; i<mv_num-1; i++) {
		if ( mv[i].hum == last_mv ) {
			//printf("43last:%d mv[i]:%d hum:%d \n", last_mv, mv[i].hum, mv[i+1].hum);
			p[ mv[i+1].hum ]++;	
		}
		
	}
	
	
	// return the one with biggest propability
	next_mv = p[PAPIER] > p[KAMIEN] ? (
			p[PAPIER] > p[NOZYCE] ? PAPIER : NOZYCE
		) : (
			p[KAMIEN] > p[NOZYCE] ? KAMIEN : NOZYCE
		);
		
	//printf(" after %d occured:P%d K%d N%d ALL:%d \n", mv[mv_num-1].hum, p[PAPIER], p[KAMIEN], p[NOZYCE], mv_num); 
	//printf(" next mv %d \n", next_mv);
	switch(next_mv) {
	case KAMIEN:
		return PAPIER;
	case PAPIER:
		return NOZYCE;
	case NOZYCE:
		return KAMIEN;
	}
	
	return 0;
	
}

char get_hum_move()
{
	char n;
		
	printf(" Twój ruch: ");
AGAIN:
	//scanf("\n%c", &n);
	/*p = fgets(n, sizeof(*n), stdin);
	if ( p == NULL ) 
		goto AGAIN;*/
	n = getch();
		
	switch(n) {
	case 'q':
		printf(" Dziękuję za gre \n");
		safe_exit("");
		break;
	case '1':
	case 'k':
		return KAMIEN;
		break;
	case '2':
	case 'p':
		return PAPIER;
		break;
	case '3':
	case 'n':
		return NOZYCE;
		break;
	default:
		printf(" Dupa jesteś źle wpisałeś, co to kurwa ma być znak '%c' spierdalaj \n", n);
		goto AGAIN;
	}
}

void gra()
{
	char *(str[]) = { 
		"kamien",
		"papier",
		"nozyce",
	};
	struct move_s tmv;
	
	tmv.com = get_com_move();
	tmv.hum = get_hum_move();
	
	printf(" ty ----- %d,%s  VS   %d,%s ------ komputer\n", 
		tmv.hum, tmv.hum == KAMIEN ? str[KAMIEN] : tmv.hum == PAPIER ? str[PAPIER] : str[NOZYCE], 
		tmv.com, tmv.com == KAMIEN ? str[KAMIEN] : tmv.com == PAPIER ? str[PAPIER] : str[NOZYCE] );
	
	if ( tmv.com == tmv.hum ) {
		printf("\t Remis! ");
		wins.even++;
	} else if (
			tmv.com == KAMIEN && tmv.hum == NOZYCE ||
			tmv.com == NOZYCE && tmv.hum == PAPIER ||
			tmv.com == PAPIER && tmv.hum == KAMIEN ) {	
		wins.com++;
		printf("\t Komputer wygrywa.");
	} else {
		printf("\t Ty wygrywasz.");
		wins.hum++;
	}
	printf("Wygrałeś już %d razy, a komputer %d razy, remis był %d razy. \n", wins.hum, wins.com, wins.even);
	
	mv_num++;
	mv = realloc(mv, mv_num*sizeof(*mv));
	if ( !mv )
		safe_exit("No memory");
	mv[mv_num-1] = tmv;

}

void initiuj()
{
	srand(time(NULL));
	
	printf(
    " Gra papier kamień nożyce, gramy \n"
		" W Grze wybierasz kamień papier lub nożyce \n"
		" a komputer gra z tobą \n"
		" Wybierz 1 lub k by wybreać kamien \n"
		" wybierz 2 lub p by grac papierem \n"
		" wybierz 3 lub n by grac nozycami\n"
		" wcisnij q by zakończyć \n"
		" gramy \n"
	);
}

int main()
{
	
	initiuj();
	
	signal(SIGHUP, sig_hup);
	signal(SIGQUIT, sig_hup);
	signal(SIGTERM, sig_hup);
	signal(SIGINT, sig_hup);
	
	for(;;) {
		gra();
	}
	return 0;
}

```
