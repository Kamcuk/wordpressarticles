---
author: kamilcukrowski
brand: xebia.com
categories:
- home
date: 2023-12-21 10:24:58+01:00
email: kamilcukrowski@gmail.com
focus-keywords: keyboard shortcuts
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/332
image: images/banner.jpg
og:
  image: images/banner.jpg
slug: my-keyboard-shortcuts
status: publish
subtitle: ''
tags:
- archlinux
- home
title: My keyboard shortcuts

---

The keyboard shortcuts that I use have evolved over the years and years of
development. They have survived switching from KDE to XFCE to ICEWM to XFCE
to KDE. They will survive till the end of time.

My goal was always to be as much compatible with existing shortcuts and only
add required functionality when needed. The idea is that when I sit in front
of someone else computer, I wouldn't have a problem using it.  The shortcuts are something
we learn naturally when using computer and when you switch environments they
should naturally work. In other words, I value intuitiveness and discoverability
much more than productivity.

[The Wikipedia article "Table of keyboard shortcuts"](
https://en.wikipedia.org/wiki/Table_of_keyboard_shortcuts
)
lists the comparisons between keyboard shortcuts of different window
management systems. The article itself is old. 15 years ago Windows did
not have shortcuts for switching virtual desktops and there wasn't really
an agreement between Linux window managers either.

For a long time now I am using [`xbindkeys`](https://wiki.archlinux.org/title/Xbindkeys)
to bind commands to certain keys. The program allowed me to switch environment
easily in my early days of programming. Nowadays, I would want to move
shortcuts to KDE shortcuting system to move to `wayland` some day in the future.
However, I do not have the time.

The shortcuts that I use are as follows:

| Shortcut                               | Action                                             | Comment
| ---                                    | ---                                                | ---
| `Ctrl`+`c`                             | open terminal                                      | I use this shortcuts exactly a million times per day
| `Ctrl`+`Fn`                            | Go to workspace n                                  |
| `Win`+`Fn`                             | Move window to workspace n.                        | Do not go to workspace n. Only move window.
| `Win`                                  | Open app menu                                      |
| `Win`+[`→`, `←`, `↑`, `↓`]             | Move window to the right/left/up/down              |
| `Win`+[`←`+`↑`, `↑`+`→`, ...]          | Move window to the upper left, upper right, etc... |
| `Win`+<code>`</code>                   | Toggle mute                                        |
| `Win`+`1`                              | Decrease volume                                    |
| `Win`+`2`                              | Increase volume                                    |
| `Win`+`3`                              | Toggle headphones vs speakers                      |
| `Win`+`4`                              | Decrease brightness                                |
| `Win`+`5`                              | Increase brightness                                |
| `Win`+`w`                              | Toggle mail program                                |
| `Win`+`e`                              | File explorer                                      |
| `Win`+`=`                              | Open my private journal                            |
| `Ctrl`+`Shift`+`Win`+`Alt`+`RightCtrl` | Suspend                                            | The shortcut is purposefully long


My `xbindkeys` configuration is [available here](
https://gitlab.com/Kamcuk/kamilscripts/-/blob/master/dotfiles/.xbindkeysrc.scm?ref_type=heads#L247
). Almost every shortcut usage should be accommodated with a visual
notification showing what is being changed. The `xbindkeys` configuration
forwards the command to some wrapper with a message that displays it using
`notify-send`.
