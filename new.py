#!/usr/bin/env python3

import argparse
import os
import re
import shlex
import subprocess
from pathlib import Path


def get_prefix():
    """
    Get the prefix of the last post
    """
    pat = re.compile(r"([0-9][0-9][0-9]*)[-_].*")
    prefixes = []
    for i in Path(".").rglob("*/"):
        res = pat.match(str(i))
        if res:
            prefixes.append(int(res.groups()[0]))
    prefixes.sort()
    return prefixes[-1]


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--dry-run", action="store_true", help="Dry run")
    parser.add_argument("title", help="post title without number with spaces")
    args = parser.parse_args()
    prefix = get_prefix() + 1
    title = args.title.capitalize()
    cmd = [
        "wp-md",
        "posts",
        "new",
        "--title",
        title,
        "--author",
        "kamilcukrowski",
        "--email",
        "kamilcukrowski@gmail.com",
        "--subtitle",
        "",
    ]
    print(f"+ {shlex.join(cmd)}")
    if not args.dry_run:
        subprocess.run(cmd, check=True)
    dir = title.replace(" ", "-").replace("_", "-").lower()
    destdir = f"{prefix}_{dir}".lower()
    print(f"+ cp {dir} {destdir}")

    if not args.dry_run:
        os.rename(dir, destdir)


if __name__ == "__main__":
    main()

# if (($# != 1)); then
# 	echo "First argument is post title without number"
# 	exit
# fi
# shopt -s extglob
# prefix=$(printf "%s\n" */ | sed -n 's/^\([0-9][0-9][0-9]*\)[-_].*/\1/p' | sort -un | tail -n1)
# prefix=$(( prefix + 1 ))
# set -xeuo pipefail
# wp-md posts new \
# 	--title "${prefix}_$1" \
# 	--author kamilcukrowski  \
# 	--brand xebia.com \
# 	--email kamilcukrowski@gmail.com \
# 	--subtitle ""
