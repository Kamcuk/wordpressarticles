---
author: kamilcukrowski
brand: kernelodyssey.ovh
categories:
- vim
date: 2023-09-13 01:12:00+02:00
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/222
slug: why-i-switched-to-neovim
status: publish
title: Why I switched to Neovim?
---

TL;DR I tried once and then I just stayed with it. Wait, what?

My journey into using Neovim as my main main editor has been a long
one. I do not fully understand that journey.

I was using vim from the beginning to edit files on Linux servers. I was using
vim on our personal server http://www.dyzio.pl and http://www.biurek.pl for
regular maintenance - editing configuration files, writing scripts, etc. Vim
was however not my main editor and back then Neovim did not exist yet.

During college I was using QT Creator. I still remember that QT Creator has
some amazing C++ refactoring capabilities. Refactoring a function definition 
from inside a class declaration in a header file to a class definition inside
a .cpp file is just one click. A really simple feature, that QT Creator is
extremely really good at. Refactoring constructors, destructors, member
functions out of and into class declarations with a blink of an eye.

When I started working, I started working on bare-metal programs. QT was way
out of the question, so QT Creator did not seem like the right choice. I
switched to Eclipse. Also because the STM32CubeMX framework was integrated with
Eclipse as an extension and generated compatible project configuration.

My fan with Eclipse lasted several years. I am happy with it. I pressed a lot
of arrow keys to navigate between symbols. Mostly used ctrl + right mouse click
to navigate to symbol definition and to find symbol declarations.

Then I discovered a meme. The meme that is the main picture of this post.
People are trying vim. And staying. And something doesn't let them out.

What was really frustrating using Eclipse is the configuration. I had to click
everything. There was no configuration history. To find some configuration I had to
navigate tons of windows. And extensibility is zero.

# Neovim

I do not know how it happened. For 2 months straight I was learning vim
ecosystem. I started with my own .vimrc configuration and tweaked it. And
tweaked it. Started learning keyboard shortcuts. And tweaked them. I wanted
full IDE experience, I wanted autocompletion, refacting, all the features and
vim seemed to support it.

I discovered vim plugins, vim-plug, tpope plugins, vim-sensible and how to
keep the configuration. My dotfiles repository also took some refactoring,
where I started using chezmoi instead of GNU stow.

However, I discovered that the language-aware capabilities of vim were not
enough. And something new, called neovim, was invented to replace it and give
it more features.

So I moved to neovim and installed coc-nvim. Coc-nvim with multiple coc-*
plugins has worked like a miracle. Finally, all the clangd features at the
palm of my hand. Not only that, but multiple languages at the palm of my hand.
2 months of writing configuration files didn't go to waste, I had now
configuration I could manage and extend at my will. I could put it into a git
and version and remember and merge between computers.

Then my transition happened from an embedded stm32 bare-metal programmer that cares
about every inch of memory to devops developer. I was now working with a lot
of servers and editing files in many of them. And, all of them have vim. Once
you learn vim, your navigation is millions times faster. Having Neovim as my
main editor, my navigation when working on remote servers increased in
magnitudes.

Not only that, I could install all the plugins at will on any
server, move my configuration with ease between machines, work remotely
with tmux that will also remember sessions on disconnects. All that works
nicely and fast. And in terminal.

I still am using Neovim. It's just great, to have something so fast, so easily
extensible, so accessible and so portable. Working on multiple machines on
multiple projects, I can login on the machine and open my editor, and each of
the machines can have their own plugins set sharing the same editor
configuration.

I am happy with my choice.
