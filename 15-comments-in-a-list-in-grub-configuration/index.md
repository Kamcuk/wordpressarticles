---
author: kamilcukrowski
brand: xebia.com
categories:
- home
date: 2023-12-10 00:52:01+01:00
email: kamilcukrowski@gmail.com
focus-keywords: list grub configuration
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/314
slug: comments-in-a-list-in-grub-configuration
status: publish
subtitle: ''
title: Comments in a list in grub configuration
tags:
- home
- archlinux
- bash
- grub
image: images/banner.jpg
og:
  image: images/banner.jpg
---

I had this list in my `/etc/default/grub`:

```
GRUB_CMDLINE_LINUX_DEFAULT="noplymouth loglevel=3 fastboot audit=0 mem_sleep_default=deep mitigations=off nvidia-drm.modeset=1 libata.noacpi=1 acpi_backlight=native nvidia.NVreg_RegistryDwords=EnableBrightnessControl=1;RMUseSwI2c=0x01;RMI2cSpeed=100 nouveau.nomodeset=0"
```

Well that is unreadable and long. Recently, I was writing my own GRUB2
personalization for my own entries, so I explored entries that I had inside my
`/etc/grub.d` directory. I noted that the `/etc/default/grub` file is just
sourced by Bash scripts. That I mean I can execute Bash commands in it and
even generate the list!

I decided to write a trivial `sed` script to allow me to add comments within
the entries in the list to know what is going on for later. Now it looks like
the following:

```
GRUB_CMDLINE_LINUX_DEFAULT="$(
    sed -n ':a;s/#.*//;N;$!ba;s/[[:space:]]\+/ /gp' <<EOF
	noplymouth
	loglevel=3
	fastboot # initialize SCSI and ATA devices asynchronously
	audit=0 # disable audit messages in journal
	mem_sleep_default=deep
	mitigations=off
	# libata.noacpi=1 # The hard drive that I bought has problems. Fix them.
	# acpi_backlight=native # Does not help for my nvidia blacklight.
	nvidia.NVreg_RegistryDwords=EnableBrightnessControl=1;RMUseSwI2c=0x01;RMI2cSpeed=100 # enable backlight and I2C on nvidia
	# nouveau.nomodeset=0 # Do not know at this time, do not use
EOF
)"
```

While the list itself is questionable, it's just great to have comments
within the file!
