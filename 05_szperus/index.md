---
author: kamilcukrowski
brand: xebia.com
date: 2015-03-11 20:47:12+02:00
email: kamilcukrowski@gmail.com
focus-keywords: szperus
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/245
slug: szperus
status: publish
subtitle: ''
title: szperus
---

Szperus - was the old script used in Warsaw Polytechnic to scan all hosts
within the local network and get all shared data.

I lost the original sources, however I want to preserve the following build
information:

Well, i don't remember origin of this but:
- make -C ./szperus/ -> compiles program named samba (yea, good name)
- this program connects on samba to a given host as a guest
- and then scan for all shared folder/files on this host
- all founded files/folders are saved to a MySQL database or to a file or both
- there are also two scripts - poll.sh and scan.sh - i don't know what their doing, we can guess from names