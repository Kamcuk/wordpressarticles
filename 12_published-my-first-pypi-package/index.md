---
author: kamilcukrowski
brand: xebia.com
date: 2023-11-19 00:00:00+01:00
email: kamilcukrowski@gmail.com
focus-keywords: published first pypi package
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/275
slug: published-my-first-pypi-package
status: draft
subtitle: ''
title: Published my first pypi package
---

Recently I published my first PyPI package available at
[nomad-tools](https://pypi.org/project/nomad-tools/).

There are several tools there that I decided to keep together for the purpose
that they keep getting interconnected. There is the main `nomad-watch` utility
that I also decided to make a post on Reddit. But there are also other
scripts.

I was really missing a simple script to show me the ports of a running Nomad
service. I was surprised that there actually is a docker tools - `docker port`
- that shows ports allocated for a particular container.