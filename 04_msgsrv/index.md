---
author: kamilcukrowski
brand: xebia.com
date: 2015-02-10 16:21:10+02:00
email: kamilcukrowski@gmail.com
focus-keywords: msgsrv
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/244
slug: msgsrv
status: publish
subtitle: ''
title: msgsrv
---

msgsrv is a messaging system server that implements:
- sending messages from clients(msgcli) to server(msgsrv)
- receiving messages from server (msgdump, msgdsp)
- sending messages with special attributes
- msgsrv pings clients, checks for activiti
- system server is used to monitor and display multiple computer
   within a local network or local vpn
- supports priorities of messages, updating of messages and more
- written by Kamil Cukrowski <kamil@dyzio.pl> and Grzegorz Sójka <grzes@sojka.co>
- feel free to use and remember about us.
- sry my bad english.

hf! hf!
