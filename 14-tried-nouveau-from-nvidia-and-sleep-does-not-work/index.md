---
author: kamilcukrowski
brand: xebia.com
date: 2023-12-10 00:41:23+01:00
email: kamilcukrowski@gmail.com
focus-keywords: nouveau nvidia
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/311
slug: tried-nouveau-from-nvidia-and-sleep-does-not-work
status: publish
subtitle: ''
title: Tried nouveau from nvidia and sleep does not work
categories:
- home
tags:
- archlinux
- nvidia
- x11
image: images/banner.jpg
og:
  image: images/banner.jpg
---

From time to time I try to discover new things. This time, like many times
before, Today I decided to delve into nouveau driver instead of the proprietary
NVIDIA. Because my memory is awful, usually I do not remember why it
worked or did not the last time, so I rediscover stuff that I already know.

Switching wasn't that easy. First I installed `xf86-driver-nouveau` on my
ArchLinux and edited `/etc/X11/xorg.conf.d/20-nvidia.conf` file as follows:

```
## https://wiki.archlinux.org/title/NVIDIA#Manual_configuration
Section "Device"
	Identifier "Nvidia Card"
	#Driver "nvidia"
	#VendorName "NVIDIA Corporation"
	#BoardName "GeForce GTX 1050 Ti"
	#Option "NoLogo" "1"
	#Option "RegistryDwords" "EnableBrightnessControl=1;RMUseSwI2c=0x01;RMI2cSpeed=100"
	Driver "nouveau"
EndSection
```

I then stopped by display manager `systemctl stop sddm` and tried from a TTY
console remove NVIDIA and start nouveau. `rmmod nouveau nvidia_drm nvidia
nvidia_uvm nvidia_modeset ; modprobe nouveau`. This did not end well, my
monitor went blank for some time, than graphical artifacts started coming up
in the form of flickering select pixels all over the screen. Well, time for
`Ctrl+PrtSc+REISUB` to reboot.

I then rebooted, even tried `blacklist nvidia` in `/etc/modprobe.conf.d/`, but
it didn't help - NVIDIA was lauded every time, and because of that nouveau
from X11 didn't want to load. I uninstalled finally `pacman -R nvidia
nvidia-settings` and regenerated initcpio and rebooted.

After reboot, oddly enough, nouveau did not load automatically. I had to
`modprobe` it myself after getting root shell on a TTY. After that, X11 picked
it up and my friendly KDE started.

However, here came the real test. I have `Shift+CtrlLeft+AltLeft+Win+AltRight+Ctrl+Right`
bound to system sleep. However, my system did not go to sleep. I have a
kernel option `mem_sleep_default=deep` enabled to go deeper into sleep, so
maybe it was its fault, but I decided not to investigate further (maybe next
time).

After that, I edited `/etc/X11/xorg.conf.d/20-nvidia.conf` back with NVIDIA
configuration and installed NVIDIA drivers and reboot.

It all started, because the brightness regulation on my monitor is
unsatisfactory. I want to decrease brightness below zero, it is constantly too
bright for my eyes. Maybe next time.
