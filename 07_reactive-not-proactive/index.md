---
author: Kamil Cukrowski
brand: kernelodyssey.ovh
categories:
- life
date: 2023-08-17 21:03:40+02:00
excerpt: Working with software reactively saves time, stress and money.
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/18
image: images/banner.jpeg
og:
  image: images/og-banner.jpg
permalink_template: https://kernelodyssey.ovh/p/18
slug: reactive-not-proactive
status: publish
title: Reactive, not proactive.
---

I had my share of work environments that I visited and worked in the past. Some were slow pacing corporate environments where no one cared about anything, maybe about what exactly was in the contract. Some were small startups where everything changed, and you had to care about everything.

The in the corporate environments and advanced projects (but also in new projects!), there was a 5+ team of people building. Scrum and flashy yellow notes with what you should do in the next two weeks. Then what should the tester do in the next two weeks. Every small change went through a lot of thinking and a lot of hands and a lot of testing. Before you released anything, there were weeks, and you were sure that something was working. Because what was really important, is not the change itself, but the contract that states that it should work.

In the small startups, I worked on the feature requested and came with my own ideas. I literally did not only anticipate errors in the code, but also what my boss `would want` to have in the code, because he wasn't enough intimate with the problems to come up with solutions. And we shipped the product, only to showcase for investors. It had to work for 5, maybe 10 minutes. We could fake historic data. Just to catch a fish on a hook.

But what I have learned, is that there is a trap laying for programmers. We, programmers, like to program for fun. There is so much open source made by programmers for programmers. So many programs made because someone made it.

# Another fish on a one way street.

We do not want our programs to stop working. So we think and imagine the most extreme of `what could possibly happen` and look both ways on a one way street. And then we look up to check for possible airplanes landing on a one way street. Then we check if this street is a pond. Anticipate every possible case. Handle the impossible. Everything, so that every case is properly handled and there is no `undefined behavior`. No loose ends.

# Proactive.

Proactive [means](https://dictionary.cambridge.org/dictionary/english/proactive): `taking action by causing change, and not only reacting to change when it happens`. As programmers, we anticipate that an airplane will land on a one way street, so just to be sure we place `no airplanes` sign. Then, just to be sure, we place a `look up for airplanes` sign before street crossings.

Do we have to do that? Will the program _ever_ handle that case? Do you always write programs for millennia and millions of use cases? Finally, how much time do you have in your hands?

While it is great to handle every possible case, it will just not happen. Time is of real value here. You can spend the time to handle every possible case. Or you can write stuff, check it works, it works, add it to technical debt, and move to the next case.

The real problem is here, how much do internally agree with yourself to call something `finished`. Programmers have many `started projects`, but why not just make it a half-assed working projects, and call it done. Why is it `started`, and just not finished? The burden that something does not handle every possible case perfectly is too much to handle, that we have to continue coding and coding to handle it? What is this burden?

# Reactive

On the other side, reactive [means](https://dictionary.cambridge.org/dictionary/english/reactive): `reacting to events or situations rather than acting first to change or prevent something`. Only handle airplanes on the street once they happen to land.

This philosophy of behaving `reactively` took me a long time to understand. And I do not think it applies exclusively to programming, but rather to the whole life in general.

Bottom line, what we have in life is we have needs. The needs have to be fulfilled. The act of coming with ideas, writing code, running code comes to fulfill a particular need. A street exists for cars to drive. That is the need.

Then, when we have a street, it works. There is no need, we can move along. Our job is done. Street done. We can move to another project. _Then_ when a pedestrian comes along, we have to come up with a crossing and rules for crossing the street. What if no pedestrian ever comes? We saved work. We saved time.

My experience is that this philosophy works great. Instead of producing programs that work in any situation, I produce programs that _fulfill the need_ they were made for. They are purposeful. Exist because of a need, not the other way round.

This creates a specific context in which the program is submerged. A reactive programmer evolves the program around external needs, is specifically hooked to what is outside of it. The program is also fluid, smooth, has low technological debt, can change quickly. Instead of being a self-fulfilling prophecy and reacting to the needs of the program itself, now the program reacts to external stimuli, changing itself when required to fill a new glass or accommodate different users.

# Golden middle

The golden middle between `reactive` and `proactive` approach is surely the solution. However, my text has been a call to programmers to work more `reactively`. Do not implement that many features that are `fun`, because you need them. Do not implement that many features that are `safe` to anticipate future errors. Let the program do its job. Think about possible situations, but not all.

And finally, when you handled the _minimum_ amount of possible situations and errors, just go away. Move along. Spend time with family. And tell it to yourself, tell it load and clear, that the project is now DONE!


