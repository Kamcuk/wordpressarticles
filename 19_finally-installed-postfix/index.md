---
author: kamilcukrowski
brand: xebia.com
categories:
- devops
date: 2024-02-25 22:29:17+01:00
email: kamilcukrowski@gmail.com
focus-keywords: Finally installed postfix
guid: https://kernelodyssey.ovh/wp-json/wp/v2/posts/370
image: images/banner.jpg
og:
  description: Finally installed postfix
  image: images/banner.jpg
slug: finally-installed-postfix
status: publish
subtitle: ''
tags:
- archlinux
- home
- nomad
title: Finally installed postfix

---

It is my 4th time that I try to install and configure an email server. And
finally, I succeed.

My home family server always has had a postfix server that my father receives
and sends his company emails. The postfix configuration is super old, at least
30 years, initially most probably has been written by my uncle. 20 years or
so, postfix MySQL plugin was added, which is a bunch of postfix redirection
scripts with hard coded MySQL statements which forward postfix account
integration into MySQL. Since a long time because of that history I wanted to
set up my own postfix on my own server.

The time has come into the 21 century that I wanted to do it. On my custom
server nowadays, I use HashiCorp Nomad and docker vitalization whenever
possible. Although using docker this is not the best choice for memory contained server and
limited budget. Each application having it's own glibc copy loads into memory
its own set of shared libraries. This results in higher memory utilization,
which with constrained memory and a lot of services can accumulate over time.

At first I tried to setup postfix on the host. This went really bad, with the
options from postfix.cf overwhelming me quite fast and the weekend went by a
long time ago.

The time has come for this weekend. After browsing through a log of different
postfix docker containers I have finally found one that with enough
configuration started working. Welcome to the era of
[`docker-mailserver`](https://docker-mailserver.github.io/docker-mailserver/latest/).

The Nomad job specification for `docker-mailserver` looks to me
straightforward. I am adding "logging" part to Nomad job specification to
redirect logs to journal - this is a single workstation without log
forwarding. The various mounts are specified in `docker-mailserver`
documentation, so they are firmly straightforward too. I also added the
mount point needed for Let'sEncrypt certificates and time.


```
locals {
  DIR = abspath(".")
}
job "mail" {
  group "mail" {
    network {
      port "SMTP" {
        host_network = "ext"
        static       = 25
      }
      port "IMAP" {
        host_network = "ext"
        static       = 143
      }
      port "SMTPS" {
        host_network = "ext"
        static       = 465
      }
      port "MSA" {
        host_network = "ext"
        static       = 587
      }
      port "IMAPS" {
        host_network = "ext"
        static       = 993
      }
    }
    task "mail" {
      driver = "docker"
      config {
        image    = "ghcr.io/docker-mailserver/docker-mailserver:latest"
        hostname = "main.kamcuk.top"
        ports    = ["SMTP", "IMAP", "SMTPS", "MSA", "IMAPS"]
        logging {
          type = "journald"
          config {
            tag = NOMAD_TASK_NAME
          }
        }
        mount {
          type     = "bind"
          source   = "${local.DIR}/data/"
          target   = "/var/mail/"
          readonly = false
        }
        mount {
          type     = "bind"
          source   = "${local.DIR}/state/"
          target   = "/var/mail-state/"
          readonly = false
        }
        mount {
          type     = "bind"
          source   = "${local.DIR}/logs/"
          target   = "/var/log/mail/"
          readonly = false
        }
        mount {
          type     = "bind"
          source   = "${local.DIR}/config/"
          target   = "/tmp/docker-mailserver/"
          readonly = false
        }
        mount {
          type     = "bind"
          source   = "/etc/letsencrypt/"
          target   = "/etc/letsencrypt/"
          readonly = true
        }
        mount {
          type     = "bind"
          source   = "/etc/localtime"
          target   = "/etc/localtime"
          readonly = true
        }
      }
      template {
        destination = "local/env"
        data        = file("./mailserver.env")
        env         = true
      }
      service {
        check {
          type     = "script"
          command  = "sh"
          args     = ["-c", "ss --listening --tcp | grep -P 'LISTEN.+:smtp' || exit 1"]
          timeout  = "3s"
          interval = "10s"
        }
      }
      resources {
        memory     = 1000
        memory_max = 10000
      }
    }
  }
}
```

I fit the configuration inside `mailserver.env` in a separate file for
readability. The `mailserver.env` file contains the [upstream
defaults](https://github.com/docker-mailserver/docker-mailserver/blob/master/mailserver.env)
with some cosmetics changes for hostname and certificates:

```
$ diff 1 mailserver.env 
14c14
< OVERRIDE_HOSTNAME=
---
> OVERRIDE_HOSTNAME="mail.kamcuk.top"
47c47
< POSTMASTER_ADDRESS=
---
> POSTMASTER_ADDRESS=kamilcukrowski@kamcuk.top
240c240
< SSL_TYPE=
---
> SSL_TYPE=manual
244,245c244,245
< SSL_CERT_PATH=
< SSL_KEY_PATH=
---
> SSL_CERT_PATH=/etc/letsencrypt/live/kamcuk.top/fullchain.pem
> SSL_KEY_PATH=/etc/letsencrypt/live/kamcuk.top/privkey.pem
```

After that, I needed to create an account. Following the `docker-mailserver`
project it was fairly easy - the developers have written an amazing collection
of scripts with `setup` as the entrypoint. With that, I could create an
account with just:

```
nomad alloc exec -job mail setup setup email add kamilcukrowski@perun.top <password>
```

However, that wasn't all that was needed for the mail to be working. Welcome
to the era of DNS. My DNS provider is [OVH](https://www.ovhcloud.com/).
However, adding the DKIM header for DNS was really easy, and it is because
the amazing documentation from `docker-mailserver` project. I followed the
configuration steps specified in [their
documentation](https://docker-mailserver.github.io/docker-mailserver/latest/config/best-practices/dkim_dmarc_spf/).
And that was it - after adding the MX and DKIM key I was done.

```
$TTL 3600
@       IN SOA dns14.ovh.net. tech.ovh.net. (2024022307 86400 3600 3600000 60)
...
     60 IN MX     10 mail
mail._domainkey        IN TXT     ( "v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv/8HPLZrw1is8h0EZZAqZEHHm7dXhxCiFkR7OtYm7M6/8+tNkg9xs6GIctPX5KacTJ9Xse8ElzmCmdkOJ4azoMQuwOU5ScvQ4np3Ifflj1iNLkYOtgQyGtsleH4MFgY5R3d32TZFDNl3Mb2lLXQbfc/M5hjKhHUiueqVNAz3ENspead+az" "9CB5Ah51qBHLxn6jUAf1Y6bBUSyAhk1uqbjc568HWgXuqCFcbqsFbF0xSxUw2pw0P/NdAW3XI1XzSbeaIq/tk6YDlzDL8+01gujA9REEr0I8Bw1GMYmnzCTDTPUiXOB+F5oaYAHnEYMFbPyvkU3mlGt3MuVY8JSOofJwIDAQAB" )
```

After that, emails were working. Yay! However, we are not done. What is worth
a good email server setup without auto configuration? It's not worth it.
Auto configuration - we have to have it.

Actually, I was surprised how simple email account auto configuration works.
When you click on Thunderbird to automatically decent email settings, the
following happens:

1. Thunderbird queries the TXT of the domain it tries to connect.
2. From the list of TXT results it greps for the one with `mailconf=` prefix

   ```
   $ dig kamcuk.top TXT | grep mailconf=
   kamcuk.top.             389     IN      TXT     "mailconf=https://autoconfig.kamcuk.top/mail/config-v1.1.xml"
   ```

3. The `mailconf=` specifies a link to an URL with XML configuration of the
   host.

I found all that by following the documentation specified in the docker
container which - you guessed it - creates and servers the XML file. Welcome
to
[`monogramm/autodiscover-email-settings`](https://hub.docker.com/r/monogramm/autodiscover-email-settings/),
which is the project recommended by `docker-mailserver` project.

Configuring and setuping up autodiscovery email settings was a breeze. After
creating the environment variables and ading additional necessery environment
variables:

```
job "autodiscover" {
  group "autodiscover" {
    network {
      port "web" { to = 8000 }
    }
    task "autodiscover" {
      driver = "docker"
      config {
        ports = ["web"]
        image = "monogramm/autodiscover-email-settings"
        logging {
          type = "journald"
          config {
            tag = NOMAD_TASK_NAME
          }
        }
      }
      template {
        destination = "local/env"
        env         = true
        data        = <<-EOF
COMPANY_NAME=kamcuk
SUPPORT_URL=https://autodiscover.example.com
DOMAIN=kamcuk.top
IMAP_HOST=main.kamcuk.top
IMAP_PORT=993
IMAP_SOCKET=SSL
SMTP_HOST=mail.kamcuk.top
SMTP_PORT=465
SMTP_SOCKET=SSL
EOF
      }
      service {
        tags = [
          "urlprefix-autoconfig.kamcuk.top:443/",
          "urlprefix-autodiscover.kamcuk.top:443/",
        ]
        port = "web"
        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "10s"
        }
      }
    }
  }
}
```

And viola - DONE. Thunderbird now automatically detects the mail configuration
and is able to connect to my own private web server. This little adventure has
made my weekend.

The next step - let's conquer LDAP. That will be another post.