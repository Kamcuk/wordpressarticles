#!/usr/bin/env python3

import argparse
import logging
import os
import shutil
import subprocess
import tempfile
from pathlib import Path

import requests
import yaml


def get_image(folder: Path, article: str):
    prompt: str = args.prompt or article[:1000]
    log.info(f"Prompt: {prompt!r}")

    dest = folder / "images"
    log.info(f"Creating {dest}")
    dest.mkdir(exist_ok=True)
    dest = dest / "banner.jpg"
    if not args.force:
        assert not dest.exists(), f"{dest} already exists"

    data = dict(
        prompt=prompt,
        n=1,
        size="256x256",
    )
    resp = requests.post(
        "https://api.openai.com/v1/images/generations",
        headers={"Authorization": f"Bearer {os.environ['OPENAI_API_KEY']}"},
        json=data,
    )
    print(resp.json())
    url = resp.json()["data"][0]["url"]
    log.info(f"Got response from OpenAI with {url}")
    with requests.get(url, stream=True) as im:
        with dest.open("wb") as f:
            shutil.copyfileobj(im.raw, f)
    log.info(f"Saved image to {dest}")
    return dest


def modify_index_md():
    meta["image"] = "images/banner.jpg"
    meta.setdefault("og", {})
    meta["og"]["image"] = "images/banner.jpg"
    with tempfile.NamedTemporaryFile("w") as f:
        log.info(f"Modyfing {index_md}")
        f.write(
            f"""\
---
{yaml.dump(meta)}
---
{article}\
"""
        )
        f.flush()
        subprocess.run(["diff", f.name, str(index_md)])
        subprocess.check_call(
            ["cp", "-v", str(index_md), f"_build/{folder.name}.backup.md"]
        )
        subprocess.check_call(["cp", "-v", f.name, str(index_md)])


if __name__ == "__main__":
    log = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description="Generate an image from a prompt")
    parser.add_argument("--prompt")
    parser.add_argument("--force", action="store_true")
    parser.add_argument("folder")
    global args
    args = parser.parse_args()
    folder = Path(args.folder)
    assert folder.is_dir(), f"{folder} is not a directory"

    index_md = folder / "index.md"
    _, metastr, article = index_md.read_text().split("---\n", 2)
    meta = yaml.safe_load(metastr)
    if not args.force:
        assert "image" not in meta, f"image already in {meta}"
        assert "image" not in meta.get(
            "og", {}
        ), f"Image already in meta['og'] = {meta['og']}"

    image = get_image(folder, article)
    modify_index_md()
    print(f"Generated image in {image}")
